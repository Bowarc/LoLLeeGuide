# League of legends Lee Sin tips
## Basics:
Lee sin is an early game champ ( ofc ) you need to get ahead quick, find the lanes that can be taken adventage of quickly, abuse thoses.

One of lee's strenghts is the adaptation.
Adapt your items to your enemies.


## Items:
- if going lethality, don't go only 1 leta item (umbral glave is good & cheap af).

- As said earlyer Umbral glaive is good
- Black cleaver is good
- Death dance has been nerfed to the ground
- Eclipse is generally a good choise
- Prowler's claw is bad? (i rly don't think so)

## Gameplays:
- ### Aggresive:  
    - #### Base runes: 
        ![](FirstStrikeBase.png "FirstStrikeBase")
    - #### Secondary runes:
        - Classic   
        ![](FirstStrikeSecondaryClassic.png     "FirstStrikeSecondaryClassic")

        - Aggresive + can rly destroy the game   
        ![](FirstStrikeSecondaryAggressive.png  "FirstStrikeSecondaryAggressive")

        - Against ap and for better late game    
        ![](FirstStrikeSecondaryVsApAndScalingOriented.png  "FirstStrikeSecondaryVsApAndScalingOriented")

        - A more tanky aproach
        j'ai oublié de noter :'(
        ![]()
    - #### Items:
        - Eclypse or Prowler's claw
        - Umbral glaive
        - Adapt
- ### Classic:
    - #### Base runes:
        ![](ConqurorBase.png "ConqurorBase")
    - #### Secondary runes:
        - Classic   
        ![](ConquerorSecondaryClassic.png "ConquerorSecondaryClassic")

        - Against ap and for better late game   
        ![](ConquerorSecondaryVsApAndScalingOriented.png  "ConquerorSecondaryVsApAndScalingOriented")
    - #### Items:
        - #### Letha
            - Eclypse or Prowler's claw
            - Umbral glaive
            - Adapt (Black Cleaver, Hexdrinker, ..)
        - #### Bruiser
            - Goredrinker
            - Black Cleaver
            - Adapt (Hexdrinker, ..)